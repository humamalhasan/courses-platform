Devise.setup do |config|
  config.secret_key = '9e3e457eb2f24fb7980abc0227b213248420828e3ae142d8ff77590b392470c37fe4f0bb5fa97895dbe24576d33fee9ffc7d630fa20434714567725ce9125147'

  config.mailer_sender = 'Homam @ Spatium <no-reply@spatium.io>'

  require 'devise/orm/active_record'


  config.case_insensitive_keys = [:email]
  config.strip_whitespace_keys = [:email]
  config.skip_session_storage = [:http_auth]
  config.stretches = Rails.env.test? ? 1 : 11
  config.reconfirmable = true
  config.expire_all_remember_me_on_sign_out = true
  config.password_length = 6..128
  config.email_regexp = /\A[^@\s]+@[^@\s]+\z/
  config.reset_password_within = 6.hours
  config.sign_out_via = :delete
  require 'omniauth-google-oauth2'
  config.omniauth  :google_oauth2, ENV['GOOGLE_APP_ID'], ENV['GOOGLE_APP_SECRET'], {access_type: "offline", approval_prompt: ""}

  require 'omniauth-facebook'
  config.omniauth :facebook, ENV['FACEBOOK_APP_ID'], ENV['FACEBOOK_APP_SECRET']

  require 'omniauth-github'
  config.omniauth :github, ENV['GITHUB_APP_ID'], ENV['GITHUB_APP_SECRET'], scope: "user.email"
end
